% Instruction schedule of one butterfly
%
% Required libraries:
% \usetikzlibrary{
%   calc,
%   backgrounds,
%   positioning,
% }

% Fill grid from coordinate (#1, #2) certain number of squares to the right
% (#3) and down (#4) using a style #5.
\newcommand{\fillgrid} [4] {
  \foreach \x [evaluate=\x as \nx using \x-1] in {1, ..., #3} {
    \foreach \y [evaluate=\y as \ny using \y-1] in {1, ..., #4} {
      \fill [gray] (#1+\nx, -#2-\ny) rectangle ++(1, -1);
    }
  }
}

\begin{tikzpicture}
  % Draw grid
  \draw [step=1, gray!30] (0, 0) grid (17, -11);

  % Fill boxes
  \begin{scope}[on background layer]
    \fillgrid{0}{0}{4}{3};
    \fillgrid{2}{3}{4}{2};
    \fillgrid{6}{5}{4}{2};
    \fillgrid{9}{7}{4}{1};
    \fillgrid{13}{8}{4}{2};
    \fillgrid{6}{10}{4}{1};
  \end{scope}

  % Clk labels
  \foreach \clk [evaluate=\clk as \x using \clk+0.5] in {0, ..., 16} {
    \node [minimum width=1, anchor=mid] (clk\clk) at (\x, 0.5) {\clk};
  }

  % Bus coordinates
  \foreach \bus [evaluate=\bus as \y using -\bus-0.5] in {0, ..., 9} {
    \node (B\bus) at (0, \y) {};
  }
  \node (b) at (0, -10.5) {};

  % Bus labels
  \node [right] at ([shift={(17, 0)}]B0) {B0};
  \node [right] at ([shift={(17, 0)}]B1) {B1};
  \node [right] at ([shift={(17, 0)}]B2) {B2};
  \node [right] at ([shift={(17, 0)}]B3) {B3};
  \node [right] at ([shift={(17, 0)}]B4) {B4};
  \node [right] at ([shift={(17, 0)}]B5) {B5};
  \node [right] at ([shift={(17, 0)}]B6) {B6};
  \node [right] at ([shift={(17, 0)}]B7) {B7};
  \node [right] at ([shift={(17, 0)}]B8) {B8};
  \node [right] at ([shift={(17, 0)}]B9) {B9};
  \node [right] at ([shift={(17, 0)}]b)  {b};

  % Instructions
  \node [left] at (B0) {\textsf{add.r} $\rightarrow$ \textsf{add.t}};
  \node [left] at (B1) {\textsf{add.r} $\rightarrow$ \textsf{ag.t}};
  \node [left] at (B2) {\textsf{add.r} $\rightarrow$ \textsf{tfg.t}};
  \node [left] at (B3) {\textsf{ag.r} $\rightarrow$ \textsf{lsur.t}};
  \node [left] at (B4) {\textsf{ag.r} $\rightarrow$ \textsf{dly11.t}};
  \node [left] at (B5) {\textsf{lsur.r} $\rightarrow$ \textsf{cmul.o}};
  \node [left] at (B6) {\textsf{tfg.r} $\rightarrow$ \textsf{cmul.t}};
  \node [left] at (B7) {\textsf{cmul.r} $\rightarrow$ \textsf{cadd.t}};
  \node [left] at (B8) {\textsf{cadd.r} $\rightarrow$ \textsf{lsuw.o}};
  \node [left] at (B9) {\textsf{dly11.r} $\rightarrow$ \textsf{lsuw.t}};
  \node [left] at (b)  {\textsf{tfg.rx2} $\rightarrow$ \textsf{cadd.o}};

  % Instruction descriptions
  % \draw [->] (clk4 |- B0) node [right] {\texttt{add.r -> add.t}}
  %         -- (clk3.east |- B0);
  % \draw [->] (clk4 |- B1) node [right] {\texttt{add.r -> ag.t}}
  %         -- (clk3.east |- B1);
  % \draw [->] (clk4 |- B2) node [right] {\texttt{add.r -> tfg.t}}
  %         -- (clk3.east |- B2);

  % \draw [->] (clk6 |- B3) node [right] {\texttt{ag.r -> lsur.t}}
  %         -- (clk5.east |- B3);
  % \draw [->] (clk6 |- B4) node [right] {\texttt{ag.r -> dly11.t}}
  %         -- (clk5.east |- B4);

  % \draw [->] (clk10 |- B5) node [right] {\texttt{lsur.r -> cmul.o}}
  %         -- (clk9.east |- B5);
  % \draw [->] (clk10 |- B6) node [right] {\texttt{tfg.r -> cmul.t}}
  %         -- (clk9.east |- B6);

  % \draw [->] (clk8 |- B7) node [left] {\texttt{cmul.r -> cadd.t}}
  %         -- (clk9.west |- B7);

  % \draw [->] (clk12 |- B8) node [left] {\texttt{cadd.r -> lsuw.o}}
  %         -- (clk13.west |- B8);
  % \draw [->] (clk12 |- B9) node [left] {\texttt{dly11.r -> lsuw.t}}
  %         -- (clk13.west |- B9);

  % \draw [->] (clk10 |- b) node [right] {\texttt{tfg.rx2 -> cadd.o}}
  %         -- (clk9.east |- b);

\end{tikzpicture}
