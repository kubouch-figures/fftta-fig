# Figures for [my FFT processor](https://gitlab.com/fft-on-tta/fft-in-tce)

[![pipeline status](https://gitlab.com/kubouch-figures/fig-fft-on-tta/badges/master/pipeline.svg)](https://gitlab.com/kubouch-figures/fig-fft-on-tta/pipelines)

Contains sources for figures used in papers etc.
Each figure has its folder and includes a `test.tex` file putting the figure in a context.
CI generates a PDF file from `test.tex` and stores the result as an [artifact](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html) that can be viewed online with an artifact browser.