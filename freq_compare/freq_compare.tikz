% Graph showing power consumption
%
% Requires pgfplots

% This downloads the data. Compile with `pdflatex --shell-escape`.
\immediate\write18{./get_results.sh}

\begin{tikzpicture}
  \tikzstyle{dataline} = [color=black, mark options=solid, mark=+]
  \begin{loglogaxis}
    [
      xlabel           = {FFT size},
      ylabel           = {FFT/mJ (norm)},
      ylabel near ticks,
      log basis x      = 2,
      grid             = both,
      major grid style = {dotted,gray!90},
      minor grid style = {dotted,gray!50},
      % ytick            = {5, 7, ..., 19},
      % minor ytick      = {5, 6, ..., 19},
      legend columns   = 6,
      legend style = {at={(0.5,1.03)}, anchor=south},
    ]
    \addlegendimage{empty legend}  % Dummy entry for the legend title
    \addplot [dataline, dashed, red]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res50mhz.csv};
    \addplot [dataline, dashed, brown]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res100mhz.csv};
    \addplot [dataline, dashed, orange]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res150mhz.csv};
    \addplot [dataline, dashed, yellow]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res200mhz.csv};
    \addplot [dataline, dashed, lime]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res250mhz.csv};
    \addplot [dataline, dashed, olive]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res300mhz.csv};
    \addplot [dataline, dashed, green]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res350mhz.csv};
    \addplot [dataline, dashed, cyan]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res400mhz.csv};
    \addplot [dataline, dashed, blue]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res450mhz.csv};
    \addplot [dataline, dashed, purple]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res500mhz.csv};
    \addplot [dataline, dashed, violet]
      table[x=FFT, y=FFT/mJ_n, col sep=comma] {res550mhz.csv};
    \legend{
      \hspace{-.6cm}\textbf{freq (MHz)},
      50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550
    }
  \end{loglogaxis}
\end{tikzpicture}
