#!/bin/bash

# Filter results stored online into smaller data sets

curl https://gitlab.com/fft-on-tta/results/raw/master/results.csv \
  | grep -E ',65,1.00,.*,mb|FFT' \
  | tee >(grep -E '16,50,|FFT' > res50mhz.csv) \
  | tee >(grep -E '16,100,|FFT' > res100mhz.csv) \
  | tee >(grep -E '16,150,|FFT' > res150mhz.csv) \
  | tee >(grep -E '16,200,|FFT' > res200mhz.csv) \
  | tee >(grep -E '16,250,|FFT' > res250mhz.csv) \
  | tee >(grep -E '16,300,|FFT' > res300mhz.csv) \
  | tee >(grep -E '16,350,|FFT' > res350mhz.csv) \
  | tee >(grep -E '16,400,|FFT' > res400mhz.csv) \
  | tee >(grep -E '16,450,|FFT' > res450mhz.csv) \
  | tee >(grep -E '16,500,|FFT' > res500mhz.csv) \
  | tee >(grep -E '16,550,|FFT' > res550mhz.csv)
