#!/bin/bash

# Filter results stored online into smaller data sets

# get raw data
CSV_DIR=csv
CSV_RAW="$CSV_DIR/raw_results.csv"
mkdir -p $CSV_DIR
curl https://gitlab.com/fft-on-tta/results/raw/master/results.csv > $CSV_RAW

# get header
HEADER=$(head -n 1 $CSV_RAW)


# mb vs ms
grep -E ',65,.*,250,' $CSV_RAW \
    | tee >(grep -E ',ms\s' > "$CSV_DIR/res_ms.csv") \
    | grep -E ',mb\s|FFT' > "$CSV_DIR/res_mb.csv"

# tech comparison for every FFT size, sort by frequency
for fft in 64 128 256 512 1024 2048 4096 8192 16384; do
    grep -E "^$fft,.*,mem,mb" $CSV_RAW \
        | tee >(grep -E ',65,1.00,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_65nm_1.00v.csv") \
        | tee >(grep -E ',28,0.60,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_28nm_0.60v.csv") \
        | tee >(grep -E ',28,0.80,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_28nm_0.80v.csv") \
        | tee >(grep -E ',28,0.85,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_28nm_0.85v.csv") \
        | tee >(grep -E ',28,0.90,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_28nm_0.90v.csv") \
        | tee >(grep -E ',28,0.95,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_28nm_0.95v.csv") \
        | tee >(grep -E ',28,1.10,16,' | sort --field-separator=',' -n -k 5,5 > "$CSV_DIR/res_${fft}_28nm_1.10v.csv")
done

# insert header to *.csv files
for f in $CSV_DIR/*.csv
do
    sed -i "1i ${HEADER}" $f
done
